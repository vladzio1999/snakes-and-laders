package com.testtask.game.model;

import com.sun.istack.NotNull;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Entity
public class Player {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotBlank
	private String username;

	@OneToOne(cascade = CascadeType.ALL)
	private Token token;

	private Integer diceValue;

	@Enumerated(value = EnumType.STRING)
	@NotNull
	private PlayerStatus status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}

	public Integer getDiceValue() {
		return diceValue;
	}

	public void setDiceValue(Integer diceValue) {
		this.diceValue = diceValue;
	}

	public PlayerStatus getStatus() {
		return status;
	}

	public void setStatus(PlayerStatus status) {
		this.status = status;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Player player = (Player) o;
		return id.equals(player.id)
				&& username.equals(player.username)
				&& Objects.equals(token, player.token)
				&& Objects.equals(diceValue, player.diceValue)
				&& status == player.status;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, username, token, diceValue, status);
	}
}
