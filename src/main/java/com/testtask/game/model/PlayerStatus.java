package com.testtask.game.model;

public enum PlayerStatus {
	WIN,
	PLAYING,
	NOT_PLAYING
}