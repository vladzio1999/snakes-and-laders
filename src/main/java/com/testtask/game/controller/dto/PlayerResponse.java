package com.testtask.game.controller.dto;

import com.testtask.game.model.Player;
import com.testtask.game.model.PlayerStatus;

public class PlayerResponse {
	private Long id;
	private String username;
	private PlayerStatus status;
	private Integer diceValue;

	public PlayerResponse(Player player) {
		this.id = player.getId();
		this.username = player.getUsername();
		this.status = player.getStatus();
		this.diceValue = player.getDiceValue();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public PlayerStatus getStatus() {
		return status;
	}

	public void setStatus(PlayerStatus status) {
		this.status = status;
	}

	public Integer getDiceValue() {
		return diceValue;
	}

	public void setDiceValue(Integer diceValue) {
		this.diceValue = diceValue;
	}
}
