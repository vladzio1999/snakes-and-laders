package com.testtask.game.controller.dto;

import com.testtask.game.model.Board;

import java.util.List;
import java.util.stream.Collectors;

public class BoardResponse {

	private Long id;
	private List<TokenResponse> tokens;

	public BoardResponse(Board board) {
		this.id = board.getId();
		this.tokens = board.getTokens().stream().map(TokenResponse::new).collect(Collectors.toList());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<TokenResponse> getTokens() {
		return tokens;
	}

	public void setTokens(List<TokenResponse> tokens) {
		this.tokens = tokens;
	}
}
