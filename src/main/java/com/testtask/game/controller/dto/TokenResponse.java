package com.testtask.game.controller.dto;

import com.testtask.game.model.Token;

public class TokenResponse {
	private Long id;
	private Integer position;
	private PlayerResponse player;

	public TokenResponse(Token token) {
		this.id = token.getId();
		this.position = token.getPosition();
		this.player = new PlayerResponse(token.getPlayer());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public PlayerResponse getPlayer() {
		return player;
	}

	public void setPlayer(PlayerResponse player) {
		this.player = player;
	}
}
