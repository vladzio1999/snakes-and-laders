package com.testtask.game.controller;

import com.testtask.game.controller.dto.PlayerResponse;
import com.testtask.game.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("players")
public class PlayerController {

	private final PlayerService playerService;

	@Autowired
	public PlayerController(PlayerService playerService) {
		this.playerService = playerService;
	}

	@PostMapping
	public PlayerResponse registerPlayer(@RequestParam String username) {
		return new PlayerResponse(playerService.create(username));
	}

	@PutMapping("/{id}/roll")
	public PlayerResponse rollDice(@PathVariable Long id) {
		return new PlayerResponse(playerService.rollDice(id));
	}
}
