package com.testtask.game.controller;

import com.testtask.game.controller.dto.BoardResponse;
import com.testtask.game.model.Board;
import com.testtask.game.service.BoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("boards")
public class BoardController {

	private final BoardService boardService;

	@Autowired
	public BoardController(BoardService boardService) {
		this.boardService = boardService;
	}

	@PostMapping
	public BoardResponse startGame(@RequestParam Long playerId) {
		Board board = boardService.create(playerId);
		return new BoardResponse(board);
	}

	@GetMapping("/{id}/play")
	private BoardResponse makeMove(@PathVariable(name = "id") Long id,
								   @RequestParam(name = "playerId") Long playerId) {
		Board board = boardService.getById(id);
		board = boardService.play(board, playerId);
		return new BoardResponse(board);
	}
}
