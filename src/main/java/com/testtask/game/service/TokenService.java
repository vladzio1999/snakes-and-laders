package com.testtask.game.service;

import com.testtask.game.model.Player;
import com.testtask.game.model.Token;
import com.testtask.game.repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenService {

	private final TokenRepository tokenRepository;

	@Autowired
	public TokenService(TokenRepository tokenRepository) {
		this.tokenRepository = tokenRepository;
	}

	public Token create(Player player) {
		Token token = new Token();
		token.setPosition(1);
		token.setPlayer(player);
		return tokenRepository.save(token);
	}

	public Token update(Token token) {
		return tokenRepository.save(token);
	}
}
