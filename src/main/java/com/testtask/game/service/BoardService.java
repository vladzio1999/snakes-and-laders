package com.testtask.game.service;

import com.testtask.game.model.Board;
import com.testtask.game.model.Player;
import com.testtask.game.model.PlayerStatus;
import com.testtask.game.model.Token;
import com.testtask.game.repository.BoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class BoardService {

	private final BoardRepository boardRepository;
	private final TokenService tokenService;
	private final PlayerService playerService;

	@Autowired
	public BoardService(BoardRepository boardRepository, TokenService tokenService, PlayerService playerService) {
		this.boardRepository = boardRepository;
		this.tokenService = tokenService;
		this.playerService = playerService;
	}

	public Board getById(Long id) {
		return boardRepository.getById(id);
	}

	public Board create(Long playerId) {
		Player player = playerService.getById(playerId);
		Token token = tokenService.create(player);
		player.setToken(token);
		player.setStatus(PlayerStatus.PLAYING);
		playerService.update(player);

		Board board = new Board();
		board.setTokens(Collections.singletonList(token));
		return boardRepository.save(board);
	}

	public Board play(Board board, Long playerId) {
		playerService.moveToken(playerId);
		return boardRepository.getById(board.getId());
	}
}
