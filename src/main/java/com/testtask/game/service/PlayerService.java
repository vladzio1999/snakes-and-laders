package com.testtask.game.service;

import com.testtask.game.model.Player;
import com.testtask.game.model.PlayerStatus;
import com.testtask.game.model.Token;
import com.testtask.game.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class PlayerService {

	private final static int WIN_POSITION = 100;
	private final static int MAX_DICE_VALUE = 6;

	private final PlayerRepository playerRepository;
	private final TokenService tokenService;

	@Autowired
	public PlayerService(PlayerRepository playerRepository, TokenService tokenService) {
		this.playerRepository = playerRepository;
		this.tokenService = tokenService;
	}

	public Player rollDice(Long playerId) {
		Player player = playerRepository.getById(playerId);

		if (player.getDiceValue() == 0) {
			player.setDiceValue(getDiceValue());

		}

		return playerRepository.save(player);
	}

	public void moveToken(Long playerId) {
		Player player = playerRepository.getById(playerId);
		Token token = player.getToken();

		if (checkIsAbleToMoveToken(token.getPosition(), player.getDiceValue())) {
			token.setPosition(token.getPosition() + player.getDiceValue());
			tokenService.update(token);
		}

		updateUserStatus(player, token.getPosition());
		player.setDiceValue(0);
		playerRepository.save(player);
	}

	public Player create(String username) {
		Player player = new Player();
		player.setUsername(username);
		player.setStatus(PlayerStatus.NOT_PLAYING);
		player.setDiceValue(0);

		return playerRepository.save(player);
	}

	public Player update(Player player) {
		return playerRepository.save(player);
	}

	public Player getById(Long id) {
		return playerRepository.getById(id);
	}

	private int getDiceValue() {
		Random random = new Random();
		return random.nextInt(MAX_DICE_VALUE) + 1;
	}

	private void updateUserStatus(Player player, int position) {
		if (position == WIN_POSITION) {
			player.setStatus(PlayerStatus.WIN);
		}
	}

	private boolean checkIsAbleToMoveToken(int tokenPosition, int diceValue) {
		return (tokenPosition + diceValue) <= WIN_POSITION;
	}
}
