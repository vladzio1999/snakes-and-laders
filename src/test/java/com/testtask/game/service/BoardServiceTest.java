package com.testtask.game.service;

import com.testtask.game.model.Board;
import com.testtask.game.model.Player;
import com.testtask.game.model.PlayerStatus;
import com.testtask.game.model.Token;
import com.testtask.game.repository.BoardRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@SpringBootTest
public class BoardServiceTest {

	@Autowired
	private BoardService boardService;

	@MockBean
	private BoardRepository boardRepository;

	@MockBean
	private PlayerService playerService;

	@Test
	void shouldCreateBoard() {
		Board expectedBoard = new Board();
		expectedBoard.setId(1L);

		Player player = new Player();
		player.setId(1L);
		player.setStatus(PlayerStatus.PLAYING);
		player.setUsername("test_user");
		player.setDiceValue(0);

		Token token = new Token();
		token.setId(1L);
		token.setPosition(1);
		token.setPlayer(player);
		player.setToken(token);

		expectedBoard.setTokens(Collections.singletonList(token));

		given(boardRepository.save(any(Board.class))).willAnswer(invocation -> invocation.getArguments()[0]);
		given(playerService.getById(player.getId())).willReturn(player);

		Board actualBoard = boardService.create(player.getId());
		actualBoard.setId(expectedBoard.getId());

		assertEquals(expectedBoard, actualBoard);
	}

	@Test
	void shouldReturnBoard() {
		Board board = new Board();
		board.setId(1L);

		given(boardRepository.getById(board.getId())).willReturn(board);

		assertEquals(board, boardService.getById(board.getId()));
	}

	@Test
	void shouldMakeAMove() {
		Board board = new Board();
		board.setId(1L);

		Long playerId = 1L;

		boardService.play(board, playerId);
		verify(playerService).moveToken(playerId);
		verify(boardRepository).getById(board.getId());
	}
}
