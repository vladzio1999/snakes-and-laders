package com.testtask.game.service;

import com.testtask.game.model.Player;
import com.testtask.game.model.PlayerStatus;
import com.testtask.game.model.Token;
import com.testtask.game.repository.TokenRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@SpringBootTest
public class TokenServiceTest {

	@Autowired
	private TokenService tokenService;

	@MockBean
	private TokenRepository tokenRepository;

	@Test
	void shouldCreateToken() {
		Player player = new Player();
		player.setId(1L);
		player.setStatus(PlayerStatus.NOT_PLAYING);
		player.setUsername("test_user");
		player.setDiceValue(0);

		Token expectedToken = new Token();
		expectedToken.setId(1L);
		expectedToken.setPlayer(player);
		expectedToken.setPosition(1);

		given(tokenRepository.save(any(Token.class))).willAnswer(invocation -> invocation.getArguments()[0]);
		Token actualToken = tokenService.create(player);
		actualToken.setId(expectedToken.getId());

		assertEquals(expectedToken, actualToken);
	}

	@Test
	void shouldUpdateToken() {
		Token token = new Token();
		tokenService.update(token);
		verify(tokenRepository).save(token);
	}
}
