package com.testtask.game.service;

import com.testtask.game.model.Player;
import com.testtask.game.model.PlayerStatus;
import com.testtask.game.model.Token;
import com.testtask.game.repository.PlayerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@SpringBootTest
public class PlayerServiceTest {

	@Autowired
	private PlayerService playerService;

	@MockBean
	private PlayerRepository playerRepository;

	@MockBean
	private TokenService tokenService;

	private Player player;

	@BeforeEach
	void setup() {
		player = new Player();
		player.setId(1L);
		player.setDiceValue(4);
		player.setUsername("test_user");
		player.setStatus(PlayerStatus.PLAYING);
	}

	@Test
	void shouldCreatePlayer() {
		player.setDiceValue(0);
		player.setStatus(PlayerStatus.NOT_PLAYING);

		given(playerRepository.save(any(Player.class))).willAnswer(invocation -> invocation.getArguments()[0]);

		Player actualPlayer = playerService.create("test_user");
		actualPlayer.setId(player.getId());

		assertEquals(player, actualPlayer);
	}

	@Test
	void shouldRollDice() {
		player.setDiceValue(0);

		given(playerRepository.getById(player.getId())).willReturn(player);
		given(playerRepository.save(any(Player.class))).willAnswer(invocation -> invocation.getArguments()[0]);

		Player updatedPlayer = playerService.rollDice(player.getId());

		assertTrue(updatedPlayer.getDiceValue() > 0 && updatedPlayer.getDiceValue() <= 6);
	}

	@Test
	void shouldMoveToken() {
		Token token = new Token();
		token.setId(1L);
		token.setPosition(1);
		token.setPlayer(player);
		player.setToken(token);

		given(playerRepository.getById(player.getId())).willReturn(player);

		ArgumentCaptor<Token> captor = ArgumentCaptor.forClass(Token.class);
		playerService.moveToken(player.getId());

		verify(tokenService).update(captor.capture());
		assertEquals(5, captor.getValue().getPosition());
	}

	@Test
	void shouldSetPlayerStatusToWin() {
		Token token = new Token();
		token.setId(1L);
		token.setPosition(96);
		token.setPlayer(player);
		player.setToken(token);

		given(playerRepository.getById(player.getId())).willReturn(player);

		ArgumentCaptor<Player> captor = ArgumentCaptor.forClass(Player.class);

		playerService.moveToken(player.getId());

		verify(playerRepository).save(captor.capture());
		assertEquals(PlayerStatus.WIN, captor.getValue().getStatus());
	}

	@Test
	void shouldNotChangePlayerStatus() {
		Token token = new Token();
		token.setId(1L);
		token.setPosition(98);
		token.setPlayer(player);
		player.setToken(token);

		given(playerRepository.getById(player.getId())).willReturn(player);

		ArgumentCaptor<Player> captor = ArgumentCaptor.forClass(Player.class);

		playerService.moveToken(player.getId());

		verify(playerRepository).save(captor.capture());
		assertEquals(PlayerStatus.PLAYING, captor.getValue().getStatus());
	}
}
