Database
--------
When you start a project, a local H2 database is automatically created, so no extra action is required. 

Available endpoints
-------------------
POST /players?username={value}  
value - Player's name you want to create
___________________
POST /boards?playerId={playerId}    
playerId - The ID of the user for whom the board will be created 
___________________
PUT /players/{id}/roll  
id - The ID of the user who rolls the dice
___________________
GET /boards/{id}/play?playerId={value}  
id - The ID of the board on which user plays    
playerId - The ID of the user who plays
___________________

Examples
--------
1. POST /players?username=John        
2. POST /boards?playerId=1 (playerId value is retrieved in response of the above request)  
3. PUT /players/1/roll     
4. GET /boards/1/play?playerId=1 (board id is retrieved in response of the second POST request)        

Later in the game, only the last two requests are used in the next order - first the PUT query, then the GET with the same parameters as described above. 